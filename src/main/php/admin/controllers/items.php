<?php
/**
 * @author		Cyprian Sniegota
 * @package		sqlreport.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controlleradmin');

class SqlreportControllerItems extends JControllerAdmin {
	public function getModel($name = 'item', $prefix = 'SqlreportModel') {
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
}
