<?php
/**
 * @author		Cyprian Sniegota
 * @package		sqlreport.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');

class SqlreportModelItems extends JModelList {
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'i.name', 'i.published'
				);
		}

		parent::__construct($config);
	}
	
	protected function getListQuery() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select( "*" );
		$query->from('#__da_slideritem i');
		return $query;
	}
}
