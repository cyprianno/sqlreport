<?php
/**
 * @author		Cyprian Sniegota
 * @package		sqlreport.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

/**
 * General Controller
 */
class SqlreportController extends JControllerLegacy {
	function display($cachable = false, $urlparams = false) {
		// set default view if not set
		JRequest::setVar('view', JRequest::getCmd('view', 'main'));
		$view = JRequest::getCmd('view', 'main');
		parent::display($cachable, $urlparams);
		// add Sub menu
		SqlreportHelper::addSubmenu($view);
	}
}
