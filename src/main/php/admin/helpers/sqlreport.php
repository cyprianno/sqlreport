<?php
/**
 * @author		Cyprian Sniegota
 * @package		sqlreport.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die;
class SqlreportHelper {
	/**
	 * Configure the submenu linkbar.
	 */
	public static function addSubmenu($submenu) {
		JSubMenuHelper::addEntry(JText::_('COM_SQLREPORT_SUBMENU_MAIN'), 'index.php?option=com_sqlreport', $submenu == 'main');
		JSubMenuHelper::addEntry(JText::_('COM_SQLREPORT_SUBMENU_ITEMS'), 'index.php?option=com_sqlreport&view=items', $submenu == 'items');
	}
}
