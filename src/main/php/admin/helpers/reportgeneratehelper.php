<?php
/**
 * @author		Cyprian Sniegota
 * @package		sqlreport.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die;
class ReportgenerateHelper {
	
	public function generate($id) {
		$sqlReport = $this->loadSqlreport($id);
		$db = JFactory::getDbo();
		$db->setQuery($sqlReport->query);
		$list = $db->loadObjectList();
		$headers = split(",", $sqlReport->header_map);
		$headersMap = array();
		foreach ($headers as $header) {
			$h = split("=", trim($header));
			$headersMap[trim($h[0])] = trim ($h[1]);
		}
		$report = array();
		if (count($list) == 0) {
			return array();
		}
		$first = $list[0];
		$reportLine = array();
		foreach ($first as $key => $val) {
			$reportLine[] = $key;
		}
		foreach ($list as $element) {
			$reportLine = array();
			foreach ($first as $key => $val) {
				$reportLine[] = $val;
			}
		}
		
	}
	
	private function loadSqlreport($id) {
		$db = JFactory::getDbo();
		$query = "select * from #__sqlreport s where s.id = ".$db->quote($id);
		$db->setQuery($query);
		$obj = $db->loadObject();
		return $obj;
	}
}