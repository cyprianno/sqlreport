<?php
/**
 * @author		Cyprian Sniegota
 * @package		sqlreport.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */
defined('_JEXEC') or die('Restricted access');
defined('DS') or define("DS","/");
require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'config.php';
// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_sqlreport')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// require helper file
JLoader::register('SqlreportHelper', dirname(__FILE__) . DS . 'helpers' . DS . 'sqlreport.php');

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Questionnaire
$controller = JControllerLegacy::getInstance('Sqlreport');

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();
