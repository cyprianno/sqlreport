<?php
/**
 * @author		Cyprian Sniegota
 * @package		sqlreport.component
 * @copyright	Copyright (C) 2011- HMail.pl Cyprian Sniegota. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die('Restricted access');
defined('DS') or define("DS","/");

jimport('joomla.application.component.controller');
$controller	= JControllerLegacy::getInstance('Sqlreport');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
